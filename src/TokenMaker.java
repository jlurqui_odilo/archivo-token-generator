import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class TokenMaker {

	private static final String HMAC_ALGO = "HmacSHA256";
	
	private static final String jsonUser = "{\"name\":null,\"username\":\"teamblue\",\"expires\":1612121076739}";	
	private static final String secretBase64 = "iAJ6uipkdV00W5nRz/IFwVJzocFUtUZnMYVrbBxDAUi+ABjgaQciMlkDIsGMk5icSoPqxoXLHdtCyPg1UGLFph5FFgqeyQ==";
	
	private final Mac hmac;

	public TokenMaker() {
		this(TokenMaker.secretBase64);
	}
	
	public TokenMaker(String secretBase64) {
        try {
            hmac = Mac.getInstance(HMAC_ALGO);
            hmac.init(new SecretKeySpec(fromBase64(secretBase64), HMAC_ALGO));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new IllegalStateException("failed to initialize HMAC: " + e.getMessage(), e);
        }
	}
	
	public static String tokenMakerCreate(String json, String secretBase64) {
		
		if (json == null) {
			json = TokenMaker.jsonUser;
		}

		if (secretBase64 == null) {
			secretBase64 = TokenMaker.secretBase64;
		}

		TokenMaker tokenMaker = new TokenMaker(secretBase64);

		return tokenMaker.getToken(json, secretBase64);
	}	

	public String getToken(String json, String secretBase64) {

		String jsonUserBase64;
		String jsonUserTokenized;
		byte[] hash = createHmac(json.getBytes());

		jsonUserBase64 = toBase64(json.getBytes());
		jsonUserTokenized = toBase64(hash);    
		
		return jsonUserBase64 + "." + jsonUserTokenized;
	}	

	private static String toBase64(byte[] content) {
        return DatatypeConverter.printBase64Binary(content);
    }

    private static byte [] fromBase64(String content) {
        return DatatypeConverter.parseBase64Binary(content);
    }

    // synchronized to guard internal hmac object
    private synchronized byte[] createHmac(byte[] content) {
        return hmac.doFinal(content);
    }
	
}
